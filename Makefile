# LaTeX Makefile

MAINFILE=main

PDFLATEX=pdflatex -interaction=batchmode
SHELL=/bin/bash
BIB=biber
TEXFILES=$(wildcard *.tex)
FIGFILES=$(wildcard fig/*.tex)
PDFFILES=$(patsubst %.tex,%.pdf,$(FIGFILES))

# Fancy stuff for echo (colors, bold...)
default=\033[0m
yellow=\e[93m
green=\e[32m
red=\e[31m
bold=\033[1m

pdf: $(MAINFILE).pdf

$(MAINFILE).pdf: $(TEXFILES) $(PDFFILES)
	@echo "Generating PDF file"; \
	$(PDFLATEX) $(MAINFILE).tex 1>/dev/null;

fig/%.pdf: fig/%.tex
	@echo -e "Generating $(yellow)$(bold)$@$(default) from $<"; \
	bn=`basename $<`; \
	file=$$bn; \
	cd fig; \
	$(PDFLATEX) $$file 1>/dev/null;

.PHONY: clean
clean:
	@echo "Cleaning..."; \
  rm -f {./,fig/}*{.log,.toc,.aux,.out}


