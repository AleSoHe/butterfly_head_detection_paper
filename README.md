# Butterfly_Head_Detection_Paper
LaTeX code for the Monarch Butterfly Head Detector, in order to submit to JMLR.

# How to compile
Just
```
make
```

# Structure

- sty/jmlr2e.sty
  - style of the JMLR journal.
- fig/
  - compilable figures
- img/
  - png/jpg images
- abstract
- acknowledgements
- appendix
- conclusions
- intro
- macros
- main
- results
- title
  - authors, title, general info

